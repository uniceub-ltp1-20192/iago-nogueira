#10.1 Use o loop for para imprimir os números de 1 a 20
print("Atividade 10.1 ")
i = list(range(1, 21))
print(i)
#10.2 Faça uma lista de de 1 a um milhão e depois imprima cada um dos valores da lista.
print("Atividade 10.3 ")
umamilhao = list(range(1, 1000001))
for i in umamilhao:
    print(i)
#10.3 Faça uma lista de de 1 a um milhão e use as funções min() e max() para grantir que sua lista realmente começa
#em 1 e termina em um milhão. Depois utilize a função sum() para imprimir a soma dos valores da lista.
print("Atividade 10.3 ")
milhao = list(range(1, 1000001))
print(min(milhao))
print(max(milhao))
print(sum(milhao))
#10.4 Use o terceiro parâmetro de função range para fazer uma lista com os números impares de 1 a 20.
# Imprima cada elemento da lista separadamente.
print("Atividade 10.4 ")
numimpar = list(range(1, 21, 2))
for i in numimpar:
    print(i)
#10.5 Faça uma lista com todos os números múltiplos de 3, no intervalo de 3 a 1000.
# Imprima cada valor separadamente.
print("Atividade 10.5 ")
lista =list(range(3, 1000, 3))
for i in lista:
    print(i)
#10.6 Um número elevado a terceira potência e chamado de cubo.
# Faça uma lista com todos os cubos entre 1 e 100, imprima cada valor separadamente
print("Atividade 10.6 ")
cubo = []
for i in range(1, 100):
    i= i**3
    print(i)
#10.7 Faça compreensão de lista para gerar a lista do exercício 10.6
print("Atividade 10.7 ")
cubo = [i**3 for i in range(1, 100)]
print(cubo)