print("Atividade 12.4: ")
#Use um loop for para imprimir cada comida que o restaurante oferece.
buffet = ('Camarão', 'Sushi', 'Churrasco', 'Leitão', 'Caranqueijo')
#Tente modificar um dos itens, e garanta que o python rejeita a alteração.
#buffet [2] = ('Cama')
#TypeError: 'tuple' object does not support item assignmen

buffet [2] = ('Cama')
print(buffet)

#O restaurante mudou o menu, trocando dois pratos do cardápio.
# Adicione um bloco de código que rescreve a tupla, e então reusa o loop for para imprimir os itens do novo menu.
buffet = ('Lasanha', 'Picanha', 'Churrasco', 'Leitão', 'Caranqueijo')
print("\nNovos itens do menu: ")
for novos in buffet:
    print(novos)