#2.1
print("Atividade 2.1:")
onion_amount = 10
onion_price = 53
print ("Você irá pagar", onion_amount*onion_price, "pelas cebolas")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#2.2
print("Atividade 2.2:")
cebolas = 300
cebolas_na_caixa = 120
espaco_caixa = 5
caixas = 60
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa/espaco_caixa
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#2.3
print("Atividade 2.3:")
cebolas = 1500
cebolas_na_caixa = 620
espaco_caixa = 370
caixas = 400
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa/espaco_caixa
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
print("\n -------------------------------------------------------------------------------------------------------------------------")
