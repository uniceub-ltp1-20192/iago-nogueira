#11.1 Slices: Escolha algum dos programas que você fez sobre listas, e adicione algumas linhas ao final do programa
# para que ele faça o seguinte:
#-----------------------------------
#Imprima a seguinte mensagem: “Os primeiros 3 itens da lista são: ”. E use um slice para imprimir os três
#primeiros itens da lista do programa escolhido.
print("Atividade 11.1: ")
nomes = ['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes', 'Iago Nogueira']
print("Os primeiros itens da lista são: ")
print(nomes[:3])

print("Os últimos 3 itens da lista são: ")
print(nomes[2:])

print("Os 3 itens no meio da lista são: ")
print(nomes[1:4])

#11.2 Crie uma lista de sabores de pizza e armazene em pizza_hut.
#Faça uma cópia da lista de pizzas e armazene em pizza_hot_paraguai. E faça o que se pede:
print("Atividade 11.2: ")
pizza_hot = ['Calabresa', 'Portguesa', 'Patifaria', 'Catupirou']
pizza_hot_paraguai = pizza_hot[:]
print(pizza_hot)
print(pizza_hot_paraguai)
pizza_hot.append('Camaleão')
pizza_hot_paraguai.append('Carne')
print(pizza_hot)
print(pizza_hot_paraguai)

print("Atividade 11.3: ")
#11.3 Quero loops, mais loops! Todos os códigos nessa nota de aula imprimiram as listas sem o uso de loops.
#Escolha um código dessa nota e imprima todos os itens como um cardápio. Use um loop para cada lista.
nomes = ['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes', 'Iago Nogueira']
for firstsname in nomes[:3]:
    print(firstsname)

for lastnames in nomes[2:]:
    print(lastnames)

for halfnames in nomes[1:4]:
    print(halfnames)