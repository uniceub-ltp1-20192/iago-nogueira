#5.1 Crie um programa que solicite um valor ao usuário e informe se o mesmo é par ou impar.
print("Atividade 1: ")
num = int(input("Informe um número: "))
if (num%2) == 0:
    print("É par")
else:
    print("É ímpar")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#5.2 Conceba uma programa que informe se um determinado número é primo.
print("Atividade 2: ")
num = int(input("Informe um número: "))
if (num%2) == 1:
    print("é primo")
else:
    print("não é primo")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#5.3 Crie uma calculadora automática que dado dois valores
# informe o resultado da adição, subtração, multiplicação e divisão.
print("Atividade 5.3: ")
num1 = int(input("Informe um número: "))
num2 = int(input("Informe um número: "))
deseja = str(input("O que quer: +, -, * ou /\n"))
print("O resultado da operação é: ")
if deseja == "+":
    print(num1+num2)
if deseja == "-":
    print(num1-num2)
if deseja == "*":
    print(num1*num2)
if deseja == "/":
    print(num1/num2)
print("\n -------------------------------------------------------------------------------------------------------------------------")
#5.4 Crie uma programa que fornecidos a idade e o semestre
# que um pessoa está informa em quantos anos ela vai se formar.
print("Atividade 5.4: ")
idade = int(input("Qual sua idade?: "))
semestre = int (input("Qual o semestre que está cursando? (1, 2, 3, 4, 5, 6, 7, 8)"))
total_semestre = int(input("Qual o total de semestre do seu curso?: "))

semestres_faltam = total_semestre - semestre
vai_formar_em = total_semestre/semestre
idade_final = idade + semestres_faltam/2

print("Você vai se formar em ", vai_formar_em, "anos. Com: ", idade_final, "anos")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#5.5 Crie uma variação do programa 5.4 que receba também a
# informação de quantos semestres o aluno está atrasado e
# indique qual o tempo total que ele vai ter no curso até se formar.
print("Atividade 5.5: ")
idade = int(input("Qual sua idade?: "))
semestre_atual = int (input("(Exercício:5.4)Qual o semestre que está cursando? (1 a 12)"))
total_semestre = int(input("(Exercício:5.4)Qual o total de semestre do seu curso?: "))
semestre_atrasado = int(input("(Exercício:5.4)Quantos semestres está atrasado? "))


semestres_faltam = total_semestre - semestre_atual
vai_formar_em = (total_semestre/semestre_atual) + (semestre_atrasado/2)
idade_final = idade + (semestres_faltam/2) + (semestre_atrasado/2)
print("Seu tempo total no curso vai ser de: ", vai_formar_em, "anos, até se forma. Com", idade_final, "anos")