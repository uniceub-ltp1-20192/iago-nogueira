#8.1 Pense em pelo menos cinco lugares no mundo que você gostaria de visitar.
#Armazene os locais em uma lista. Certifique­se de que a lista não esteja em ordem alfabética.
lugares = ['Ceilândia', 'Samambaia', 'Planaltina', 'Sobradinho', 'Guará']
#Imprima sua lista na ordem original. Não se preocupe em imprimir a lista item por item, basta imprimi­la como uma lista bruta do Python.
print(lugares)
#Use sorted() para imprimir sua lista em ordem alfabética sem modificar a lista atual.
print("\nLista com sorted: ")
print(sorted(lugares))
#Mostre que sua lista ainda está em sua ordem original, imprimindo­a.
print("\nLista original: ")
print(lugares)
#Use sorted() para imprimir sua lista em ordem alfabética inversa sem alterar a ordem da lista original.
print("\nLista alfabética inversa: ")
print(sorted(lugares, reverse=True))
print(lugares)
#Use reverse() para alterar a ordem da sua lista. Imprima a lista para mostrar que a ordem foi alterada.
print("\n")
print(lugares)
lugares.reverse()
print(lugares)
#Use reverse() para alterar a ordem da sua lista novamente. Imprima a lista para mostrar que ela está de volta ao pedido original.
print("\n")
lugares.reverse()
print(lugares)
#Use sort() para que a lista seja armazenada em ordem alfabética. Imprima a lista para mostrar que a ordem foi alterada.
print("\n")
lugares.sort()
print(lugares)
#Use sort() para que a lista seja armazenada em ordem alfabética inversa. Imprima a lista para mostrar que a ordem foi alterada.
lugares.sort(reverse=True)
print(lugares)
print("\n -----------------------------------------------------------------------")
#8.2 Altere os códigos desenvolvidos para o convite de jantar (Notas de aula 7) para que seja apresentada a quantidade de convidados existentes.
print("Atividade 8.2: ")
convidados =['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Olá, " + convidados[0] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[1] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[2] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[3] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("\n")
convidados.insert (0, "Patrícia")
convidados.insert (2, "Renata Nogueira")
convidados.append ("Zailton Nogueira")
print("\n")
print(convidados)
convidados.append("XXXXXXXXXXXXXX")
print(convidados)
print("Convidados Existentes: ")
print(len(convidados))

#8.3 Todas as funções!
letras = ['X', 'G', 'W', 'A', 'B', 'C']
print(letras)
print("\nUsando somente sort")
letras.sort()
print(letras)


print("\nUsando sort com reverse=True:")
print(letras)
letras.sort(reverse=True)
print(letras)

print("\nUsando sorted:")
print(sorted(letras))

print("\nUsando sorted com o parâmetro reverse=True:")
print(sorted(letras, reverse=True))

print("\nUsando reverse: ")
letras.reverse()
print(letras)

print("\nUsando reverse novamente para a lista voltar ao que era antes do primeiro reverse: ")
letras.reverse()
print(letras)

print("\nUsando len: ")
letras = ['X', 'G', 'W', 'A', 'B', 'C']
print(len(letras))