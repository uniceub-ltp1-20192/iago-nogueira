#9.1 Implemente um código em Python que some todos elementos dentro de uma lista.
print("Atividade 9.1: ")
lista = ['1', '2', '3']
soma = 0
for numero in lista:
    soma += int(numero)
    print(soma)
print("o último valor da lista é o resultado da adição")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#9.2 Implemente um código em Python que multiplique todos elementos dentro de uma lista.
print("Atividade 9.2: ")
lista = ['1', '2', '3']
mult = 1
for numero in lista:
    mult *= int(numero)
    print(mult)
print("o último valor da lista é o resultado da multiplicação")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#9.3 Escreva um código que retorne o menor elemento de uma lista.
print("Atividade 9.3: ")
lista = ['9', '2', '3']
print(min(lista))
print("\n -------------------------------------------------------------------------------------------------------------------------")
#9.4 Escreva um código que fornecidas duas listas imprima uma lista com os elementos comuns entre elas.
print("Atividade 9.4: ")
ListaA = ['abc', 'ghi', 'ywz']
ListaB = ['ceb', 'ywz', 'abc']
comuns = list(set(ListaA) & set(ListaB))
print(comuns)
print("\n -------------------------------------------------------------------------------------------------------------------------")
#9.5 Implemente um programa que imprima todos elementos de uma lista em ordem alfabética
print("Atividade 9.5: ")
ListaA = ['Ronaldinho Gaúcho', 'Lionel Messi', 'Cristiano', 'Neymar']
ListaA.sort()
print(ListaA)
print("\n -------------------------------------------------------------------------------------------------------------------------")

#9.6 Implemente um código que imprima uma lista com o tamanho de cada elemento da lista de entrada.
print("Atividade 9.6: ")
ListaA = ['Ronaldinho Gaúcho', 'Lionel Messi', 'Cristiano', 'Neymar']
for i in ListaA:
    print(len(i))