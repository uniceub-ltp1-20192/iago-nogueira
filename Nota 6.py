#6.1 Armazene os nomes de alguns de seus amigos em uma lista chamada nomes
# Imprima o nome de cada pessoa acessando cada elemento da lista, um de cada vez.
print("Atividade 6.1: ")
nomes = ['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print(nomes[0])
print(nomes[1])
print(nomes[2])
print(nomes[3])
print("\n -------------------------------------------------------------------------------------------------------------------------")
#6.2 Comece com a lista que você criou no exercício 6.1
# mas em vez de apenas imprimir o nome de cada pessoa
# imprima uma mensagem para ela. O texto de cada mensagem
# deve ser o mesmo, mas cada mensagem deve conter o nome da pessoa específica.
print("Atividade 6.2: ")
nomes = ['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Essa mulher é louca, " + nomes[0] + ".")
print("A mais pertubada, " + nomes[1] +".")
print("Novinha sensacional, " + nomes[2] + ".")
print("A velha que já desencalhou, mas agora não, " + nomes[3] + ".")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#6.3 Pense no seu modo de transporte favorito, como uma moto ou um carro
# e faça uma lista que armazene modelos do seu transporte favorito
# Use sua lista para imprimir uma série de mensagens sobre esses itens
# como: "Eu gostaria de ter uma moto da Honda".
print("Atividade 6.3: ")
nissan = ['Nissan March', 'Nissan Versa', 'Nissan Sentra', 'Nissan Frontier', 'Nissan GT-R']
print("Mensagens que a Nissan utiliza para cada modelo: ")
print("Quanto mais ágil melhor, " + nissan[0] + ".")
print("O carro para quem sonha grande, " + nissan[1] + ".")
print("O novo héroi das ruas chegou, " + nissan[2] + ".")
print("Tecnologia e força para qualquer desafio, " + nissan[3] + ".")
print("Redefina seu conceito de velocidade, " + nissan[4] + ".")