#7.1 Se você pudesse convidar alguém, vivo ou morto, para jantar, quem você convidaria?
#Faça uma lista que inclua pelo menos três pessoas que você gostaria de convidar para o jantar.
#Em seguida, use sua lista para imprimir uma mensagem para cada pessoa, convidando­a para jantar.
print("Exercício 7.1:")
convidados =['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Olá, " + convidados[0] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[1] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[2] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[3] + ", você acaba de receber um convite para jantar.")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#7.2 Acabou de ouvir que um dos seus convidados não pode fazer o jantar
#por isso precisa de enviar um novo conjunto de convites. Você terá que pensar em outra pessoa para convidar.
#• Comece com o seu programa do exercício 7.1. Adicione uma declaração de impressão no final do programa,
# informando o nome do convidado que não pode comparecer.   ,
print("Exercício 7.2:")
convidados =['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Olá, " + convidados[0] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[1] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[2] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[3] + ", você acaba de receber um convite para jantar.")
print("\n")
print("Infelizmente, " + convidados[3] + "e " + convidados[2] + ", não poderão comparecer!")
print("\n")
convidados[3] = "Bruna Alarcão"
convidados[2] = "Maria Helena"
print("Olá, " + convidados[0] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[1] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[2] + ", você acaba de receber um convite para jantar.")
print("Olá, " + convidados[3] + ", você acaba de receber um convite para jantar.")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#7.3 Você acabou de encontrar uma mesa de jantar maior, então agora há mais espaço disponível.
#Pense em mais três convidados para convidar para o jantar.
print("Exercício 7.3:")
convidados =['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Olá, " + convidados[0] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[1] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[2] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[3] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("\n")
convidados.insert (0, "Patrícia")
convidados.insert (2, "Renata Nogueira")
convidados.append ("Zailton Nogueira")
print("Olá, " + convidados[0] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[1] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[2] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[3] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[4] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[5] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("\n -------------------------------------------------------------------------------------------------------------------------")
#7.4 Você acabou de descobrir que sua nova mesa de jantar não chegará a tempo para o jantar
#e terá espaço para apenas dois convidados.
print("Exercício 7.4:")
convidados =['Milenna Armando', 'Samara Ferreira', 'Duda Gontijo', 'Larissa Fernandes']
print("Olá, " + convidados[0] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[1] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[2] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("Olá, " + convidados[3] + ", encontrei uma mesa de jantar maior, então temos mais lugares disponíveis, mudaremos de mesa.")
print("\n")
convidados.insert (0, "Patrícia")
convidados.insert (2, "Renata Nogueira")
convidados.append ("Zailton Nogueira")
print("\n")
print(convidados)
print("\n")
print("Olá, " + convidados[0] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[1] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[2] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[3] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[4] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[5] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("Olá, " + convidados[6] + ", você foi convidado para jantar na mesa 18, onde será muito bem acomodado.")
print("\n")
print("Infelizmente a mesa não chegará a tempo, só terá espaço para dois convidados")
convidado_removido = convidados.pop()
print("Infelizmente não posso te convidar para jantar, " + convidado_removido + ".")
convidado_removido = convidados.pop()
print("Infelizmente não posso te convidar para jantar, " + convidado_removido + ".")
convidado_removido = convidados.pop()
print("Infelizmente não posso te convidar para jantar, " + convidado_removido + ".")
convidado_removido = convidados.pop()
print("Infelizmente não posso te convidar para jantar, " + convidado_removido + ".")
convidado_removido = convidados.pop()
print("Infelizmente não posso te convidar para jantar, " + convidado_removido + ".")
print("\n")
print("Olá, " + convidados[0] + ", você ainda está convidada")
print("Olá, " + convidados[1] + ", você ainda está convidada")
del convidados[0]
del convidados[0]
print(convidados)